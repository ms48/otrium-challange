@extends('layouts.main')
@section('title', 'Turnover report')
@section('content')
<!-- Page Heading -->
<!--<h1 class="h3 mb-2 text-gray-800">Turnover Report</h1>-->
@if ($errors->any())
<div class="alert alert-danger error_wrapper">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h5 class="m-0 font-weight-bold text-primary">Turnover Report (Day 01 to Day 07)</h5>
            </div>
            <div class="col-xs-12 col-sm-4 btn-wrapper">
                <button id="btn_export" class="btn btn-outline-primary" disabled="">
                    <i class="fas fa-file-csv"></i> Export
                </button>
            </div>
        </div>    
    </div>    
    <div class="card-body"> 
        <form>
            <input type="hidden" value="{{ csrf_token() }}" id="_token">
            <input type="hidden" value="{{ url('/') }}" id="url">
            <div class="row">
                <div class="col-xs-12 col-sm-5 mt-1">                     
                    <select class="form-control select2-year filter" id="year">
                        <option></option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-5 mt-1">
                    <select class="form-control select2-month filter" id="month">
                        <option></option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-2 mt-1">
                    <button type="button" id="btn_search" class="btn btn-outline-primary">
                        <i class="fas fa-search"></i> Search
                    </button>
                </div>
            </div>
        </form>
        <div id="table_data"></div>
        
    </div>
</div>
@endsection
@section('script')
<script src="js/custom.js"></script>
@endsection