@if (count($data) > 0 )
<div class="mt-4">
    <table class="table table-striped table-responsive" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Brand</th>
                <th>Day1</th>
                <th>Day2</th>
                <th>Day3</th>
                <th>Day4</th>
                <th>Day5</th>
                <th>Day6</th>
                <th>Day7</th>
                <th>Total Turnover</th>
                <th>Total excluded VAT ({{ config('tax.vat_percentage') }}%)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $row)
            <tr>
                <td>{{$row['brand']}}</td>
                <td>{{number_format($row['day1'], 2)}}</td>
                <td>{{number_format($row['day2'], 2)}}</td>
                <td>{{number_format($row['day3'], 2)}}</td>
                <td>{{number_format($row['day4'], 2)}}</td>
                <td>{{number_format($row['day5'], 2)}}</td>
                <td>{{number_format($row['day6'], 2)}}</td>
                <td>{{number_format($row['day7'], 2)}}</td>
                <td>{{number_format($row['total_by_brand'], 2)}}</td>
                <td>{{number_format($row['total_without_vat'], 2)}}</td>
            </tr>
            @endforeach  

        </tbody>
    </table>
</div>
@endif