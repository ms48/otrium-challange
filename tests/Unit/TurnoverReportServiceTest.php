<?php
namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\TurnoverReportService;
use App\Repositories\GmvRepositoryInterface;
use App\Services\CsvService;
use App\Exceptions\NoDataException;

class TurnoverReportServiceTest extends TestCase
{
    protected $turnoverReportService;
    protected $gmvRepository;
    protected $csvService;


    public function setUp(): void
    {
        parent::setUp();
        $this->gmvRepository = $this->getMockBuilder(GmvRepositoryInterface::class)
            ->setMethods(['getTurnoverSummaryReport'])->getMock();
        
        $this->csvService = $this->getMockBuilder(CsvService::class)
            ->setMethods(['getCsv'])->getMock();
        $this->turnoverReportService = new TurnoverReportService($this->gmvRepository,$this->csvService );
    }

    public function testGetTurnoverSummaryData()
    {
        $year = 2018;
        $month = 2;

        $reportData = [['brand' => 'aaa'], ['brand' => 'bbb'], ['brand' => 'ccc']];
        
        $this->gmvRepository->expects($this->once())
            ->method('getTurnoverSummaryReport')->willReturn($reportData);
        
        $result = $this->turnoverReportService
            ->getTurnoverSummaryData($year, $month);

        $this->assertEquals($result, $reportData);
    }
    
    public function testGenerateCsvNoDataFoundExceptionWhenDataIsEmpty()
    {
        $year = 2018;
        $month = 2;
        
        $this->expectException(NoDataException::class);
        $this->turnoverReportService
            ->generateCsv($year, $month);
    }    
    
}
