<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TurnoverReportController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


Route::get('/', [TurnoverReportController::class, 'index']);
Route::get('report/export', [TurnoverReportController::class, 'export']);
Route::post('report/sub_table', [TurnoverReportController::class, 'getSubTable']);
