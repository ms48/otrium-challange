<?php

namespace App\Repositories\Eloquent;

use App\Repositories\GmvRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use App\Models\Gmv;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class GmvRepository implements GmvRepositoryInterface
{

    protected Model $gmvModel;


    public function __construct(Gmv $gmvModel)
    {
         $this->gmvModel = $gmvModel;
    }

     
    /**
     * Get turnover summary pivot data for first 7 days of the month
     * 
     * @param int $year
     * @param int $month
     * 
     * @return array
     */
    public function getTurnoverSummaryReport(int $year, int $month): array
    {
        $startDate = Carbon::createMidnightDate($year, $month, 1)->toDateTimeString();
        $endDate = Carbon::create($year, $month, 7, 23, 59, 59)->toDateTimeString();
        $data =  $this->gmvModel->select(
            'brands.name AS brand',
            DB::raw(
                'COALESCE(SUM(CASE WHEN DAY(gmv.date)=1 THEN gmv.turnover END),0) AS day1'
            ),
            DB::raw(
                'COALESCE(SUM(CASE WHEN DAY(gmv.date)=2 THEN gmv.turnover END),0) AS day2'
            ),
            DB::raw(
                'COALESCE(SUM(CASE WHEN DAY(gmv.date)=3 THEN gmv.turnover END),0) AS day3'
            ),
            DB::raw(
                'COALESCE(SUM(CASE WHEN DAY(gmv.date)=4 THEN gmv.turnover END),0) AS day4'
            ),
            DB::raw(
                'COALESCE(SUM(CASE WHEN DAY(gmv.date)=5 THEN gmv.turnover END),0) AS day5'
            ),
            DB::raw(
                'COALESCE(SUM(CASE WHEN DAY(gmv.date)=6 THEN gmv.turnover END),0) AS day6'
            ),
            DB::raw(
                'COALESCE(SUM(CASE WHEN DAY(gmv.date)=7 THEN gmv.turnover END),0) AS day7'
            ),
            DB::raw(
                'SUM(gmv.turnover) AS total_by_brand'
            ),
            DB::raw(
                sprintf(
                    "ROUND(SUM(gmv.turnover) * 100/(100+%d), 2) AS total_without_vat",
                    config('tax.vat_percentage')
                )
            )
        )
        ->join('brands', 'gmv.brand_id', 'brands.id')
        ->whereBetween('gmv.date', [$startDate, $endDate])
        ->groupBy('brands.name')
        ->orderBy('brands.name')
        ->get();
        
        return ($data) ? $data->toArray() : [];
    }
}
