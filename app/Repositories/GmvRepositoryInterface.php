<?php

namespace App\Repositories;

interface GmvRepositoryInterface
{
    
   
  /**
   * Get turnover summary pivot data for first 7 days of the month
   * 
   * @param int $year
   * @param int $month
   * 
   * @return array
   */
    public function getTurnoverSummaryReport(int $year, int $month):array;
    
    
}