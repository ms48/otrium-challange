<?php
namespace App\Http\Controllers;

use App\Services\TurnoverReportService;
use App\Http\Requests\ReportDataRequest;
use App\Http\Requests\ExportDataRequest;
use Illuminate\Support\Facades\Redirect;
use App\Exceptions\NoDataException;

class TurnoverReportController extends Controller
{

    protected TurnoverReportService $reportService;

    public function __construct(TurnoverReportService $reportService)
    {
        $this->reportService = $reportService;
    }

    /**
     * Home page view
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Export CSV report
     *
     * @param ExportDataRequest $request
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function export(ExportDataRequest $request)
    {
        //get validated data
        $validatedData = $request->validated();

        //set default values
        $year = $validatedData['year'];
        $month = $validatedData['month'];
        
        try{
            $result = $this->reportService->generateCsv($year, $month);
            if (!$result) {
                //no data found for export
                return Redirect::back()->withErrors(['Sorry! No data to export']);
            }
        }catch(NoDataException $exception){
            return Redirect::back()->withErrors([$exception->getMessage()]);
        }

        return $result;
    }

    /**
     * Display sub view data
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function getSubTable(ReportDataRequest $request)
    {
        //get validated data
        $validatedData = $request->validated();

        $data = $this->reportService->getTurnoverSummaryData(
            $validatedData['year'],
            $validatedData['month']
        );
        return view("sub_view.table", compact('data'))->render();
    }
}
