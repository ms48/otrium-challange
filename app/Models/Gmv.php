<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gmv extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gmv';

    /**
     * The primary key.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    //relationships
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id', 'id');
    }
}
