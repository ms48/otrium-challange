<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\GmvRepositoryInterface;
use App\Repositories\Eloquent\GmvRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GmvRepositoryInterface::class, GmvRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
