<?php

namespace App\Services;

use \Symfony\Component\HttpFoundation\StreamedResponse;
use App\Exceptions\NoDataException;

class CsvService
{

    /**
     * Make CSV from array of data
     *
     * @param array $data CSV data array.
     * @param array $columns Columns data
     * @param string $fileName File name
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     * @throws NoDataException
     */ 
    public function getCsv(array $data, array $columns = [], string $fileName = 'file'): StreamedResponse
    {
        if(!$data){
            throw new NoDataException();
        }
        
        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=" . $fileName . ".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];
        
        if (!$columns) {
            //get column names by array keys
            $columns = array_keys($data[0]);
        }
        
        //output file as stream
        $callback = function() use ($columns, $data) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach ($data as $row) {
                fputcsv($file, $row);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
}
