<?php

namespace App\Services;

use App\Repositories\GmvRepositoryInterface;
use App\Services\CsvService;
use Carbon\Carbon;
use App\Exceptions\NoDataException;

class TurnoverReportService
{
    /**
     * Gmv repository contract
     * 
     * GmvRepositoryInterface GmvRepositoryInterface
     */
    protected GmvRepositoryInterface $gmvRepository;
    
    /**
     * Csv related methods
     * 
     * CsvService CsvService
     */
    protected CsvService $csvService;

    public function __construct(GmvRepositoryInterface $gmvRepository, CsvService $csvService)
    {
        $this->gmvRepository = $gmvRepository;
        $this->csvService = $csvService;
    }

    /**
     * Get summary data for report
     *
     * @param int $year Year value
     * @param int $month Month Value
     *
     * @return array
     */
    public function getTurnoverSummaryData(int $year, int $month): array
    {
        return $turnoverReport = $this->gmvRepository
            ->getTurnoverSummaryReport($year, $month);
    }

    /**
     * Generate turnover CSV
     *
     * @param int $year Year value
     * @param int $month Month Value
     *
     * @return mixed
     */
    public function generateCsv(int $year, int $month)
    {
        //get report data
        $turnoverReport = $this->getTurnoverSummaryData($year, $month);
        if (!$turnoverReport) {
            throw new NoDataException();
        }        

        //make array with total row
        $data = $this->makeArrayWithTotals($turnoverReport);
        
        $titles = $this->makeTitlesArray($year, $month);
       
        $reportName = 'turnover_report(1-' . $month . '-' . $year . ' to 7-' . $month . '-' . $year;
       
        //generate csv
        return $this->csvService->getCsv(
                $data,
                $titles,
                $reportName
        );
    }
    
    public function makeTitlesArray(int $year, int $month): array
    {
        //get month string
        $monthStr = Carbon::create($year, $month)->format('F');
        
        return [
            'Brand',
            $monthStr . ' - 1',
            $monthStr . ' - 2',
            $monthStr . ' - 3',
            $monthStr . ' - 4',
            $monthStr . ' - 5',
            $monthStr . ' - 6',
            $monthStr . ' - 7',
            'Sub total - per brand',
            sprintf("Sub total - per brand (Excluded %d%% VAT)", config('tax.vat_percentage'))
        ];
    }

    /**
     * Make pivot array from data
     *
     * @param array $turnoverReportData Data from table
     *
     * @return array An array for CSV
     */
    protected function makeArrayWithTotals(array $turnoverReportData): array
    {
        //make pivot table with total
        $totalsRow = ['Sub total - per day'];
        foreach ($turnoverReportData as $row) {
            for ($i = 1; $i <= 7; $i++) {
                if (isset($totalsRow['day' . $i])) {
                    $totalsRow['day' . $i] += $row['day' . $i];
                } else {
                    $totalsRow['day' . $i] = $row['day' . $i];
                }
            }
            if (isset($totalsRow['total_by_brand'])) {
                $totalsRow['total_by_brand'] += $row['total_by_brand'];
            } else {
                $totalsRow['total_by_brand'] = $row['total_by_brand'];
            }

            if (isset($totalsRow['total_without_vat'])) {
                $totalsRow['total_without_vat'] += $row['total_without_vat'];
            } else {
                $totalsRow['total_without_vat'] = $row['total_without_vat'];
            }
        }

        //add totals to the array
        $turnoverReportData[] = $totalsRow;
        
        return $turnoverReportData;
    }
}
