<?php

namespace App\Exceptions;

use Exception;

class NoDataException extends Exception
{
    
    public function __construct(string $message = "No Data Found!")
    {
        parent::__construct($message);
    }
}
