
$( document ).ready(function() {
    // Call select2
    $(".select2-year").select2({
        placeholder: "Select a year",
        theme: 'bootstrap4',
        allowClear: true
    });
    
    $(".select2-month").select2({
        placeholder: "Select a year",
        theme: 'bootstrap4',
        allowClear: true
    });
    
    $('#btn_search').prop("disabled", true);
    $('#btn_export').prop("disabled", true);
    
    //export data
    $('#btn_export').on('click', function(e){
        e.preventDefault();
        var year = $('#year').val();
        var month = $('#month').val();
        
        if(!year || !month){
            alert('Please select year and month to continue');
            return false;
        }
        var url = $('#url').val();
        location.href = url + '/report/export?year=' + year + '&month=' + month;
    });
    
    //send form data
    $('#btn_search').on('click', function(){
        var year = $('#year').val();
        var month = $('#month').val();
        var token = $('#_token').val();
        var url = $('#url').val();
        
        if(!isValid()){
            return false;
        }
        
        $.post(url + "/report/sub_table", {year: year, month: month, _token: token})
            .done(function (data) {
                if(!data){
                    $('#btn_export').prop("disabled", true); 
                    $('#table_data').html('<div class="mt-4"><div class="alert alert-warning mt-4"><strong>Sorry!</strong> No Product Found.</div></div>');
                }else{    
                    $('#btn_export').prop("disabled", false); 
                    $('#table_data').html(data);
                }
            });
    });
    

    $('.filter').on('change', function(){
        if(!isValid()){
            return false;
        }
        $('#btn_search').attr("disabled", false);
    });
    
    
    function isValid(){
        var year = $('#year').val();
        var month = $('#month').val();
        
        if(!year || !month){
            return false;
        }
        
        return true;
    }
    
    
});

